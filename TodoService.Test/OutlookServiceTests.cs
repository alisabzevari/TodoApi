using System;
using System.Collections.Generic;
using Xunit;
using System.Threading.Tasks;
using Moq;
using TodoService.Outlook;
using TodoService.Models;
using System.Net.Http;
using RichardSzalay.MockHttp;
using Newtonsoft.Json;
using System.Net;

namespace TodoService.Test
{
    public class OutlookServiceTests
    {
        private IEnumerable<TodoTask> fakeTodoTasks;
        private IEnumerable<TodoEvent> fakeTodoEvents;

        public OutlookServiceTests()
        {
            this.fakeTodoTasks = FakeEntities.GetFakeTodoTasks();

            this.fakeTodoEvents = FakeEntities.GetFakeTodoEvents();
        }

        [Fact]
        public async void GetTodayTasksAsync_Should_SendGetRequestToCorrectUrl()
        {
            var token = "fake-token";
            var tomorrowMorning = DateTime.UtcNow.Date.AddDays(1).ToString("o");
            var mockMessageHandler = new MockHttpMessageHandler();
            var responseEnvelope = new ODataEnvelope<IEnumerable<TodoTask>>() { Value = this.fakeTodoTasks };            
            mockMessageHandler.When(HttpMethod.Get, "https://outlook.office.com/api/v2.0/me/tasks")
                .WithQueryString("$select", "Id,Subject,DueDateTime")
                .WithQueryString("$filter", $"DueDateTime/DateTime le '{tomorrowMorning}'")
                .WithHeaders("Authorization", $"Bearer {token}")
                .Respond("application/json", JsonConvert.SerializeObject(responseEnvelope));
            
            var outlookService = new OutlookService(new HttpClient(mockMessageHandler));
            outlookService.AccessToken = token;

            var result = await outlookService.GetTodayTasksAsync();

            Assert.Equal(this.fakeTodoTasks, result.LeftOrDefault());
        }

        [Fact]
        public async void GetTodayEventsAsync_Should_SendGetRequestToCorrectUrl()
        {
            var token = "fake-token";
            var tomorrowMorning = DateTime.UtcNow.Date.AddDays(1).ToString("o");
            var todayMorning = DateTime.UtcNow.Date.ToString("o");
            var mockMessageHandler = new MockHttpMessageHandler();
            var responseEnvelope = new ODataEnvelope<IEnumerable<TodoEvent>>() { Value = this.fakeTodoEvents };
            mockMessageHandler.When(HttpMethod.Get, "https://outlook.office.com/api/v2.0/me/events")
                .WithQueryString("$select", "Id,Subject,Start,End")
                .WithQueryString("$filter", $"(Start/DateTime ge '{todayMorning}' and Start/DateTime le '{tomorrowMorning}') or (End/DateTime ge '{todayMorning}' and End/DateTime le '{tomorrowMorning}')")
                .WithHeaders("Authorization", $"Bearer {token}")
                .Respond("application/json", JsonConvert.SerializeObject(responseEnvelope));

            var outlookService = new OutlookService(new HttpClient(mockMessageHandler));
            outlookService.AccessToken = token;

            var result = await outlookService.GetTodayEventsAsync();

            Assert.Equal(this.fakeTodoEvents, result.LeftOrDefault());
        }
    }
}
