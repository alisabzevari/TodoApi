using System;
using System.Collections.Generic;
using TodoService.Models;

namespace TodoService.Test
{
    public class FakeEntities
    {
        public static List<TodoTask> GetFakeTodoTasks()
        {
            return new List<TodoTask>()
            {
                new TodoTask() { Id = "id-1", Subject = "subject-1" },
                new TodoTask() { Id = "id-2", Subject = "subject-2" }
            };
        }

        public static List<TodoEvent> GetFakeTodoEvents()
        {
            var start = new TodoTime() { DateTime = DateTimeOffset.Parse("2017-01-01"), TimeZone = "UTC" };
            var end1 = new TodoTime() { DateTime = DateTimeOffset.Parse("2017-01-01T01:01:00"), TimeZone = "UTC" };
            var end2 = new TodoTime() { DateTime = DateTimeOffset.Parse("2017-01-03"), TimeZone = "UTC" };
            return new List<TodoEvent>()
            {
                new TodoEvent() { Id = "id-1", Subject = "subject-1", Start = start, End = end1 },
                new TodoEvent() { Id = "id-2", Subject = "subject-2", Start = start, End = end2 }
            };
        }

        public static TodosBundle GetFakeTodosBundle()
        {
            return new TodosBundle() 
            {
                Events = GetFakeTodoEvents(),
                Tasks = GetFakeTodoTasks()
            };
        }
    }
}
