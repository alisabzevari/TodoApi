using System;
using System.Collections.Generic;
using TodoService.Controllers;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Moq;
using TodoService.Models;
using TodoService.Outlook;
using System.Net.Http;
using TodoService.Utils;
using System.Net;

namespace TodoService.Test
{
    public class TodosControllerTests
    {
        private List<TodoTask> fakeTodoTasks;
        private List<TodoEvent> fakeTodoEvents;
        private TodosBundle fakeTodosBundle;

        public TodosControllerTests()
        {
            this.fakeTodoEvents = FakeEntities.GetFakeTodoEvents();
            this.fakeTodoTasks = FakeEntities.GetFakeTodoTasks();
            this.fakeTodosBundle = FakeEntities.GetFakeTodosBundle();

        }
        private Mock<IOutlookService> getMockOutlookServiceWithOkResult()
        {
            Mock<IOutlookService> mock;

            mock = new Mock<IOutlookService>();
            mock.Setup(service => service.GetTodayTasksAsync())
                .Returns(Task.FromResult(new Either<List<TodoTask>, ErrorResponse>(this.fakeTodoTasks)));
            mock.Setup(service => service.GetTodayEventsAsync())
                .Returns(Task.FromResult(new Either<List<TodoEvent>, ErrorResponse>(this.fakeTodoEvents)));

            return mock;
        }

        [Fact]
        public async void GetTodayTodosAsync_Returns_OkObjectResult_InNormalSituations()
        {
            var controller = new TodosController(this.getMockOutlookServiceWithOkResult().Object);

            var result = await controller.GetTodayTodosAsync();

            var objectResult = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        public async void GetTodayTodosAsync_Returns_TodosBundleProvidedByOutlookServiceInResult_InNormalSituations()
        {
            var controller = new TodosController(this.getMockOutlookServiceWithOkResult().Object);

            var result = await controller.GetTodayTodosAsync();

            var bundleResult = Assert.IsAssignableFrom<TodosBundle>((result as OkObjectResult)?.Value);
            Assert.Equal(bundleResult, this.fakeTodosBundle);
        }

        [Fact]
        public async void GetTodayTodosAsync_Must_PassAuthorizationHeaderToOutlookService()
        {
            var token = "fake-token";
            var mockOutlookService = this.getMockOutlookServiceWithOkResult();
            var controller = new TodosController(mockOutlookService.Object);

            var result = await controller.GetTodayTodosAsync($"Bearer {token}");

            mockOutlookService.VerifySet(service => service.AccessToken);
        }

        [Fact]
        public async void GetTodayTodosAsync_Must_ReturnUnauthorizedWhenEventsEndpointOfOutlookServiceCallWasUnauthorized()
        {
            var mockOutlookService = new Mock<IOutlookService>();
            mockOutlookService.Setup(service => service.GetTodayTasksAsync())
                .Returns(Task.FromResult(new Either<List<TodoTask>, ErrorResponse>(this.fakeTodoTasks)));

            mockOutlookService.Setup(service => service.GetTodayEventsAsync())
                .Returns(Task.FromResult(new Either<List<TodoEvent>, ErrorResponse>(new ErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized"))));
            var controller = new TodosController(mockOutlookService.Object);

            var result = await controller.GetTodayTodosAsync();

            Assert.Equal(401, (result as ObjectResult).StatusCode.Value);
        }

        [Fact]
        public async void GetTodayTodosAsync_Must_ReturnUnauthorizedWhenTasksEndpointOfOutlookServiceCallWasUnauthorized()
        {
            var mockOutlookService = new Mock<IOutlookService>();
            mockOutlookService.Setup(service => service.GetTodayEventsAsync())
                .Returns(Task.FromResult(new Either<List<TodoEvent>, ErrorResponse>(this.fakeTodoEvents)));

            mockOutlookService.Setup(service => service.GetTodayTasksAsync())
                .Returns(Task.FromResult(new Either<List<TodoTask>, ErrorResponse>(new ErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized"))));
            var controller = new TodosController(mockOutlookService.Object);

            var result = await controller.GetTodayTodosAsync();

            Assert.Equal(401, (result as ObjectResult).StatusCode.Value);
        }
    }
}
