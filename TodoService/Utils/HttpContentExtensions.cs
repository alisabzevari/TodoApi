using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TodoService.Utils
{
    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsAsync<T>(this HttpContent content)
        {
            var dataStr = await content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(dataStr);
        }
    }
}
