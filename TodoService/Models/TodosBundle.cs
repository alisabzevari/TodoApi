using System;
using System.Collections.Generic;
using System.Linq;

namespace TodoService.Models
{
    public class TodosBundle: IEquatable<TodosBundle>
    {
        public IEnumerable<TodoTask> Tasks { get; set; }
        public IEnumerable<TodoEvent> Events { get; set; }

        public bool Equals(TodosBundle other)
        {
            if (other == null)
                return false;
            return Enumerable.SequenceEqual(this.Events, other.Events) && Enumerable.SequenceEqual(this.Tasks, other.Tasks);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as TodosBundle);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.Events.GetHashCode();
                hash = hash * 23 + this.Tasks.GetHashCode();
                return hash;
            }
        }

    }
}
