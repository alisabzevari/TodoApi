using System;

namespace TodoService.Models
{
    public class TodoTask : IEquatable<TodoTask>
    {
        public string Id { get; set; }
        public string Subject { get; set; }

        public bool Equals(TodoTask other)
        {
            if (other == null)
                return false;
            return this.Id.Equals(other.Id) && this.Subject.Equals(other.Subject);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as TodoTask);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.Id.GetHashCode();
                hash = hash * 23 + this.Subject.GetHashCode();
                return hash;
            }
        }
    }
}
