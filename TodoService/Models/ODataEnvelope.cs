namespace TodoService.Models
{
    public class ODataEnvelope<T>
    {
        public T Value { get; set; }
    }
}
