using System;

namespace TodoService.Models
{
    public class TodoEvent : IEquatable<TodoEvent>
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public TodoTime Start { get; set; }
        public TodoTime End { get; set; }

        public bool Equals(TodoEvent other)
        {
            if (other == null)
                return false;
            return this.Id.Equals(other.Id) && 
                this.Subject.Equals(other.Subject) &&
                this.Start.Equals(other.Start) &&
                this.End.Equals(other.End);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as TodoEvent);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.Id.GetHashCode();
                hash = hash * 23 + this.Subject.GetHashCode();
                hash = hash * 23 + this.Start.GetHashCode();
                hash = hash * 23 + this.End.GetHashCode();
                return hash;
            }
        }

    }
}
