using System;

namespace TodoService.Models
{
    public class TodoTime : IEquatable<TodoTime>
    {
        public DateTimeOffset DateTime { get; set; }
        public string TimeZone { get; set; }

        public bool Equals(TodoTime other)
        {
            if (other == null)
                return false;
            return this.DateTime.Equals(other.DateTime) && this.TimeZone.Equals(other.TimeZone);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as TodoTime);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.DateTime.GetHashCode();
                hash = hash * 23 + this.TimeZone.GetHashCode();
                return hash;
            }
        }
    }
}
