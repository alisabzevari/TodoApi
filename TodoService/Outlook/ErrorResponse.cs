using System.Net;

namespace TodoService.Outlook
{
    public class ErrorResponse
    {
        public ErrorResponse(HttpStatusCode statusCode, string reason)
        {
            this.StatusCode = statusCode;
            this.Reason = reason;
        }
        public HttpStatusCode StatusCode { get; set; }
        public string Reason { get; set; }
    }
}
