using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TodoService.Models;
using TodoService.Utils;


namespace TodoService.Outlook
{
    using TasksEitherError = Either<List<TodoTask>, ErrorResponse>;
    using TasksEnvelope = ODataEnvelope<List<TodoTask>>;
    using EventsEitherError = Either<List<TodoEvent>, ErrorResponse>;
    using EventsEnvelope = ODataEnvelope<List<TodoEvent>>;
    
    public class OutlookService : IOutlookService
    {
        private readonly HttpClient httpClient;
        public OutlookService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public string AccessToken { get; set; }

        public async Task<EventsEitherError> GetTodayEventsAsync()
        {
            var tomorrowMorning = DateTime.UtcNow.Date.AddDays(1).ToString("o");
            var todayMorning = DateTime.UtcNow.Date.ToString("o");

            this.configureHeaders();
            var result = await httpClient.GetAsync($"https://outlook.office.com/api/v2.0/me/events?$select=Id,Subject,Start,End&$filter=(Start/DateTime ge '{todayMorning}' and Start/DateTime le '{tomorrowMorning}') or (End/DateTime ge '{todayMorning}' and End/DateTime le '{tomorrowMorning}')");

            return result.IsSuccessStatusCode
                ? (await result.Content.ReadAsAsync<EventsEnvelope>()).Value
                : (EventsEitherError) new ErrorResponse(result.StatusCode, result.ReasonPhrase);
        }

        public async Task<TasksEitherError> GetTodayTasksAsync()
        {
            var tomorrowMorning = DateTime.UtcNow.Date.AddDays(1).ToString("o");

            this.configureHeaders();
            var result = await httpClient.GetAsync($"https://outlook.office.com/api/v2.0/me/tasks?$select=Id,Subject,DueDateTime&$filter=DueDateTime/DateTime le '{tomorrowMorning}'");

            return result.IsSuccessStatusCode
                ? (await result.Content.ReadAsAsync<TasksEnvelope>()).Value
                : (TasksEitherError) new ErrorResponse(result.StatusCode, result.ReasonPhrase);
        }

        private void configureHeaders()
        {
            this.httpClient.DefaultRequestHeaders.Clear();
            if (!string.IsNullOrEmpty(this.AccessToken))
            {
                this.httpClient.DefaultRequestHeaders
                    .Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken); 
            }
        }
    }
}
