using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TodoService.Models;
using TodoService.Utils;

namespace TodoService.Outlook
{
    public interface IOutlookService
    {
        string AccessToken { get; set; }
        Task<Either<List<TodoTask>, ErrorResponse>> GetTodayTasksAsync();
        Task<Either<List<TodoEvent>, ErrorResponse>> GetTodayEventsAsync();
    }
}
