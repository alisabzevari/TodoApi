using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoService.Models;
using TodoService.Outlook;
using System.Net.Http;
using System.Net.Http.Headers;

namespace TodoService.Controllers
{
    [Route("api/[controller]")]
    public class TodosController : Controller
    {
        private readonly IOutlookService outlookService;

        public TodosController(IOutlookService outlookService)
        {
            this.outlookService = outlookService;
        }

        [HttpGet("today")]
        public async Task<IActionResult> GetTodayTodosAsync(
            [FromHeader(Name = "Authorization")] string authorizationHeader = null
        )
        {
            if (!string.IsNullOrEmpty(authorizationHeader))
            {
                var parts = authorizationHeader.Split(' ');
                if (parts.Length == 2)
                {
                    this.outlookService.AccessToken = authorizationHeader.Split(' ')[1];
                }
            }

            var tasks = await this.outlookService.GetTodayTasksAsync();
            var events = await this.outlookService.GetTodayEventsAsync();

            return tasks.Match(
                t => events.Match(
                    e => Ok(new TodosBundle() { Tasks = t, Events = e }),
                    error => this.StatusCode((int)error.StatusCode, error.Reason)
                ),
                error => this.StatusCode((int)error.StatusCode, error.Reason)
            );
        }
    }
}
